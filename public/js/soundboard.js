function initSoundboard() {

    var sb = {
        song: null,
        init: function () {
            sb.song = new Audio();
            sb.listeners();
        },
        listeners: function () {
            $(document).click(sb.stop);
            $(".btnsound").click(sb.play);
        },
        play: function (e) {
            sb.song.src = e.target.value;
            sb.song.play();
        },
        stop: function (e) {
            if(e.target.name == "test") {
            }
            else{
                console.log(sb.song);
                sb.song.src = '';
                }
        }
    };
    sb.init();
}
