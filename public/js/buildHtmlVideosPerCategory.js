function initBuildHtmlVideosPerCategory(){
var categoryId;
var categoryName;
$(document).ready(function(){
    $('.category').click(function(){
        if (categoryId == $(this).attr("data-categoryId")){
            categoryName = $(this).attr("data-categoryName");
            //$(document.getElementById("collapseHeader").innerHTML) = "Video's van de categorie: " + categoryName;
            $(".collapse").toggle();
        }else{
            categoryId = $(this).attr("data-categoryId");
            if (categoryId){
                categoryName = $(this).attr("data-categoryName");
                //var x = $(document.getElementById("collapseHeader"));
                //x.innerHTML = "Video's van de categorie: " + categoryName;
                $.ajax({
                    url: '/videos/get/' + categoryId,
                    type: "GET",
                    datatype: "JSON",
                    success: function (data) {
                        var sortedJson = [];
                        for (var i in data)
                        {
                            sortedJson.push(data[i]);
                        }
                        sortedJson.sort((a,b) => Number(b.rating) - Number(a.rating));
                        //console.log(sortedJson);
                        $('.moviesPerCategory').empty();
                        var html = "";
                        //html+= '<h1>' + "Video\'s van de categorie: " + categoryName + '<\h1>';
                        //html+= '<div class="row moviesPerCategory">';
                        $.each(sortedJson, function (key, value) {
                            //alert(value.movie_url);
                            html += '<div class="col">' ;
                            html += '<div class="media">' ;
                            html += '<div class="media-body">' ;
                            html += '<a href="#" class="vidCatAnchor" data-toggle="modal" data-target="#videoModal" data-src="storage/videos/' + value.movie_url + '"  title="' + value.description + '">' ;
                            html += '<img class="img-thumbnail videosPerCategoryImage" id="playVidImage" src="/img/playVidImage.jpg" alt="' + value.description + '">' ;
                            html += '</a>' ;
                            html += '</div>' ;
                            html += '</div>' ;
                            html += '<a href="#" class="vidCatAnchor" data-toggle="modal" data-target="#videoModal" data-src="storage/movies/' + value.movie_url + '"><h4>' + value.title + '</h4></a>' ;
                            html += '</div>';
                        });
                        //html += '</div>';
                        //alert(html);
                        $('.moviesPerCategory').append(html);
                    },
                    error: function(xhr, status, text){
                        console.log(status + ' ' + text);
                    },
                    complete:function(){
                        $(".collapse").show();
                    }
                });
            }
        }
    });
});

}
