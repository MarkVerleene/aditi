function initPlayVideoInModal() {
    $(document).ready(function() {
        $('.videosPerCategoryImage').click(function(){console.log("a")});
        // Gets the video src from the data-src on each image
        var $videoSrc;
        /* var x  = document.getElementsByClassName('videosPerCategoryImage');
         $('.videosPerCategoryImage').click(function(e) {
             console.log(e);
             $videoSrc = $(this).data( "src" );
             console.log($videoSrc);
         });*/

        // when the modal is opened autoplay it
        $('#videoModal').on('shown.bs.modal', function (e) {
            //console.log(e.relatedTarget.attributes[4].nodeValue);
            var $getSrc = e.relatedTarget.attributes[4].nodeValue;
            $videoSrc = $getSrc;
            //alert($videoSrc);
            // set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
            $("#video").attr('controls',false );
            $("#video").attr('src',$getSrc );
        });

        // stop playing the youtube video when I close the modal
        $('#videoModal').on('hide.bs.modal', function (e) {
            // a poor man's stop video
            $("#video").attr('src','');

        });

        $('#video').on('ended', function (e) {
            $('#videoModal').modal('hide');
        });
        $('#video').click (function (e) {
            if (video.paused)
                video.play();
            else
                video.pause();
        })

// document ready
    });
}
