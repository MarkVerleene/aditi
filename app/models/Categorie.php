<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    protected $guarded = [];

    public static function getCategories(){
        $categories = Categorie::all();

        return $categories;
    }

    public function movies()
    {
        return $this->hasMany('App\models\Movie', 'id', 'categorie_id');

    }

}
