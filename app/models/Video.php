<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Nexmo\Message\Query;

class Video extends Model
{
    protected $guarded = [];

    public static function getRandomMoviesByRating(){

        $maxNumberOfMovies = "10";
        $randomMoviesByRating = Video::all()->random($maxNumberOfMovies)->sortByDesc("rating");
        return $randomMoviesByRating;

    }

    public function categorie()
    {
        return $this->belongsTo('App\models\Categorie', 'categorie_id', 'id');

        //return $this->belongsTo(Video::class);
    }

    public function scopeSearch($query, $q){
        if($q == null) return $query;
        return $query
            ->where('title', 'LIKE', "%{$q}%")
            ->orWhere('categorie_id', 'LIKE', "%{$q}%")
            ->orWhere('rating', 'LIKE', "%{$q}%");

    }

}
