<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $guarded = [];

    public static function getRandomMoviesByRating(){
        $maxNumberOfMovies = "10";
        $randomMoviesByRating = Movie::all()->random($maxNumberOfMovies)->sortByDesc("rating");
        return $randomMoviesByRating;

    }

    public function categorie()
    {
        return $this->belongsTo('App\models\Categorie', 'categorie_id', 'id');

        //return $this->belongsTo(Movie::class);
    }
}
