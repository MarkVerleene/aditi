<?php

namespace App\Http\Controllers;

use App\models\Link;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use App\models\Categorie;
use App\models\video;
use App\models\Info;

class PagesController extends Controller
{
    //
    public function index()
    {
        $categories = Categorie::getCategories();

        return view('homepage', compact('categories'));
    }

    public function getMoviesByCategory($id){
        $maxNumberOfMovies = "10";
        $moviesByCategory = Video::all()->where('categorie_id', $id);
        $numberOfMovies = $moviesByCategory->count();

        if($numberOfMovies < $maxNumberOfMovies){

            $maxNumberOfMovies = $numberOfMovies;
        };

        $randomMoviesByCategory = $moviesByCategory->random($maxNumberOfMovies);

        return $randomMoviesByCategory;
    }

    public function showPenis(){

        return view('pictures/penis');
    }

    public function showVagina(){

        return view('pictures/vagina');
    }

    public function showInfo(){

        $infos = Info::all();
        $links = Link::all();
        return view('info', compact('links', 'infos'));
    }

    public function showLogin(){

        return view('login');
    }

    /*public function showAdmin(){

        return view('admin');
    }*/
}
