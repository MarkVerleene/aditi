<?php

namespace App\Http\Controllers\Admin;

use App\models\Info;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class InfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $infos = Info::all();
        return view('admin.info.index', compact('infos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.info.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        Info::create($request->all());
        return redirect()->route('admin.info.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Info $info
     * @return Response
     */
    public function show(Info $info)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Info $info
     * @return Response
     */
    public function edit(Info $info)
    {
        return view('admin.info.edit', compact('info'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Info $info
     * @return Response
     */
    public function update(Request $request, Info $info)
    {
        $info->update($request->all());
        return redirect()->route('admin.info.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Info $info
     * @return Response
     */
    public function destroy(Info $info)
    {
        $info->delete();
        return redirect()->route('admin.info.index');
    }
}
