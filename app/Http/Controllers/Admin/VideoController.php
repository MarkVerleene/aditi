<?php

namespace App\Http\Controllers\Admin;

use App\models\Video;
use App\models\Categorie;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $sortBy = 'id';
        $orderBy = 'desc';
        $perPage = 20;
        $q = null;

        if($request->has('orderBy')) $orderBy = $request->query('orderBy');
        if($request->has('sortBy')) $sortBy = $request->query('sortBy');
        if($request->has('perPage')) $perPage = $request->query('perPage');
        if($request->has('q')) $q = $request->query('q');


        $videos = Video::search($q)->orderBy($sortBy, $orderBy)->paginate($perPage);
        return view('admin.videos.index', compact('videos','orderBy', 'sortBy', 'q' ,'perPage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.videos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //dd($request);

        if($file = $request->file('videoFile')){
            $name = $file->getClientOriginalName();
            $category = (string)$request->request->get('categorie_id');
            $title = $request->request->get('title');

            $uniqueName = $title . $category . $name;
            //dd($uniqueName);
            //if($file->move('videos', $uniqueName)){
                if($path = $request->file('videoFile')->storeAs('public\videos', $uniqueName)){
                $request->request->add(['movie_url' => $uniqueName]);

                Video::create($request->except('videoFile'));
                return redirect()->route('admin.videos.index');
            };
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return Response
     */
    public function show(Video $video)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return Response
     */
    public function edit(Video $video)
    {

        return view('admin.videos.edit', compact('video'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  \App\Video  $video
     * @return Response
     */
    public function update(Request $request, Video $video)
    {
        $video->update($request->all());
        return redirect()->route('admin.videos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return Response
     */
    public function destroy(Video $video)
    {
        //dd($video);
        $fileToDel = $video->getAttributeValue('movie_url');
        //dd('public/videos/'.$fileToDel);
        Storage::delete('public\videos/'.$fileToDel);

        $video->delete();

        return redirect()->route('admin.videos.index');
    }
}
