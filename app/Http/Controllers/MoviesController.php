<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\models\movie;

class

MoviesController extends Controller
{
    //
    public function index(){


        //toon x (=10) random van alle movies hoogste rating eerst
        $randomMoviesByRating = Movie::getRandomMoviesByRating();

        return view('movies.index', compact('randomMoviesByRating'));
    }

    public function show(Movie $movie){

        return view('movies.show', compact('movie'));
    }

    public function create(){
        //laad de upload videofile view
        //return view('movies.create');
    }

    public function store(Request $request){
        //Sla videoFile op
        //echo $request;

        //$attributes = request()->all();

        //$title=$request->input('title');
        //$path=$request->file('videoFile')->storeAs('public',$title);
        //return redirect ('/movies');
    }

    /*public function getVideo(Movie $movie){
        $id = $movie->id;
        $fileContents = Storage::disk('local')->get("public/{$id}");
        $response = Response::make($fileContents, 200);
        $response->header('Content-Type', "video/mp4");
        return $response;
    }*/



}
