<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index')->name('SeksEd');

Route::get('/videos/get/{id}', 'PagesController@getMoviesByCategory');

Route::get('/info', 'PagesController@showInfo');
Route::get('/vagina', 'PagesController@showVagina');
Route::get('/penis', 'PagesController@showPenis');

Route::get('/info/admin', 'PagesController@showAdmin');

//Route::resource('/movies', 'MoviesController');

//Route::post('/movies', 'MoviesController@store');

//Route::get('get-video/{video}', 'MoviesController@getVideo')->name('getVideo');

/*Route::get('/upload/something', [
    'as' => 'movie',
    'uses' => 'MoviesController@show'
]);*/


Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->prefix('admin')->name('admin.')->group(function(){
    Route::resource('videos', 'Admin\VideoController');
    Route::resource('links', 'Admin\LinkController');
    Route::resource('info', 'Admin\InfoController');
    Route::get('/laravel-filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
    Route::post('/laravel-filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');
});
