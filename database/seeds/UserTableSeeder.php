<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'username' => 'Admin',
            'password' => bcrypt('secret'),
            'remember_token' => "",
        ]);
        DB::table('users')->insert([
            'username' => 'Topse',
            'password' => bcrypt('cret'),
            'remember_token' => "",
        ]);
    }
}
