<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class categoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categories')->insert([
            'name' => "Manueel",
            'icon' => "/img/seksualiteit/masturbatie.png",
            'audio' => "sounds/bel.mp3",

        ]);

        DB::table('categories')->insert([
            'name' => "Oraal",
            'icon' => "/img/seksualiteit/oral.png",
            'audio' => "sounds/click.mp3",

        ]);

        DB::table('categories')->insert([
            'name' => "Penetratie",
            'icon' => "/img/seksualiteit/penetratie.png",
            'audio' => "sounds/sos.mp3",

        ]);

        DB::table('categories')->insert([
            'name' => "Holebi",
            'icon' => "/img/seksualiteit/holebi.png",
            'audio' => "http://upload.wikimedia.org/wikipedia/commons/6/6f/Cello_Live_Performance_John_Michel_Tchaikovsky_Violin_Concerto_3rd_MVT_applaused_cut.ogg",

        ]);
    }
}
