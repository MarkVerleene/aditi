<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    {{--required meta-tags--}}
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', 'Default')</title>

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

</head>
<body>
<header>
    @yield('header')
</header>

<div class="container">
    @yield('content')
</div>

</body>

<footer class="page-footer font-small mdb-color lighten-3 pt-4">
    <div class="container">
        <hr>
        <div class="d-flex justify-content-center">
            <div class="row ">
                <div class="d-flex align-items-center">

                    <div class="col-6">
                        <a href="http://aditivzw.be/nl/" target="_blank"><img class="img-fluid footerImage" src="{{asset('img/logos/logo_aditi.png')}}" alt="Aditi vzw"/></a>
                    </div>

                    <div class="col-6">
                        <a href="https://www.ucll.be/" target="_blank"><img class="img-fluid footerImage" src="{{asset('img/logos/logo_ucll.png')}}" alt="UCLL" /></a>
                    </div>

                </div>
            </div>
        </div>

        <div class="footer-copyright text-center py-3">
            © 2019 Copyright<a href=""> </a>
            <p>Deze website is gemaakt door Verleene Mark in samenwerking met Enckels Liza, Hermans Lotte en Lenskens Marten</p>
        </div>

    </div>
</footer>

<script src="{{ mix('js/app.js') }}" type="text/javascript" ></script>

@yield('scriptsToLoad')

</html>
