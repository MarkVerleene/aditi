@extends('layouts.adminlayout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Video aanpassen</div>

                <div class="card-body">

                    <form method="POST" action="{{ route('admin.videos.update', $video->id) }}">
                        {{ method_field('PUT') }}
                        @csrf
                        Title:
                        <input type="text" name="title" value="{{$video->title}}" class="form-control" />
                        Categorie:
                        <div class="form-group">
                            {{--                                <label for="sel1">Select list:</label>--}}
                            <select class="form-control" id="selectCategory" name="categorie_id">
                                <option value="{{$video->categorie_id}}" selected></option>
                                <option value="1">Manueel</option>
                                <option value="2">Oraal</option>
                                <option value="3">Penetratie</option>
                                <option value="4">Holebi</option>
                            </select>
                        </div>
                        Beschrijving:
                        <input type="text" name="description" value="{{$video->description}}" class="form-control" />
                        Rating:
                        <input type="text" name="rating" value="{{$video->rating}}" class="form-control" />
                        <br />
                        Videobestandsnaam:
                        <label>{{$video->movie_url}}</label>

                        <br />

                        <input type="submit" value="Bewaar" class="btn btn-primary"/>
                        <a value="Annuleer" class="btn btn-primary btn-close" href="{{route('admin.videos.index')}}"> Annuleer</a>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
