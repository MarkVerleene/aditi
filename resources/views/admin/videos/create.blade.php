@extends('layouts.adminlayout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Nieuwe video toevoegen</div>

                <div class="card-body">

                    <form method="POST" action="{{route('admin.videos.store')}}" enctype="multipart/form-data" name="videoFileForm">
                        @csrf
                        <div class="field">
                            Video bestand:

                            <input type="file" name="videoFile" id="inputFile" value="" class="input" />
                            <br />
                            Title:
                            <input type="text" name="title" class="form-control" />
                            Categorie:
{{--                            <input type="text" name="categorie_id" class="form-control" />--}}

                            <div class="form-group">
{{--                                <label for="sel1">Select list:</label>--}}
                                <select class="form-control" id="selectCategory" name="categorie_id">
                                    <option value="1">Manueel</option>
                                    <option value="2">Oraal</option>
                                    <option value="3">Penetratie</option>
                                    <option value="4">Holebi</option>
                                </select>
                            </div>

                            Beschrijving:
                            <input type="text" name="description" class="form-control" />
                            Rating:
                            <input type="text" name="rating" class="form-control" />

                            <br />

                            <input type="submit" value="Bewaar" id="createVideoFileFormSubmitBtn" class="btn btn-primary"/>
                            <a value="Annuleer" class="btn btn-primary btn-close" href="{{route('admin.videos.index')}}"> Annuleer</a>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
