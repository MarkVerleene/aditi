@extends('layouts.adminlayout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Video's</div>

                <div class="card-body">

                    <a href="{{route('admin.videos.create')}}" class="btn btn-sm btn-primary">Nieuwe video toevoegen</a>
                    <br /><br />

                    <form action="{{route('admin.videos.index')}}">
                        <div class="row">
                            <div class="col-md-4">
                                <input class="form-control form-control-sm" type="search" name="q" value="{{ $q }}">
                            </div>

                            <div class="col-md-2 col-3">
                                <select name="sortBy" class="form-control form-control-sm" value="{{ $sortBy }}">
                                    @foreach(['title','categorie_id','rating'] as $col)
                                        <option @if($col==$sortBy) selected @endif value="{{$col}}">{{ucfirst($col)}}</option>
                                        @endforeach
                                    </select>
                            </div>

                            <div class="col-md-2 col-3">
                                <select name="orderBy" class="form-control form-control-sm" value ="{{$orderBy}}">
                                    @foreach(['asc','desc'] as $order)
                                        <option @if($order==$orderBy) selected @endif value="{{$order}}">{{ucfirst($order)}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-2 col-3">
                                <select name="perPage" class="form-control form-control-sm" value ="{{$perPage}}">
                                    @foreach(['20','50','100','250'] as $page)
                                        <option @if($page==$perPage) selected @endif value="{{$page}}">{{ucfirst($page)}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-2 col-3">
                                <button type="submit" class="w-100 btn btn-sm btn-primary">Filter</button>
                            </div>

                        </div>
                    </form>

                    <table class="table">
                        <tr>
                            <th>Titel</th>
                            <th>Categorie</th>
                            <th>Rating</th>
                            <th></th>
                        </tr>
                        @forelse($videos as $video)
                            <tr>
                                <td>
                                    {{ $video->title }}
                                </td>
                                <td>
                                    {{ $video->categorie_id }}
                                </td>
                                <td>
                                    {{ $video->rating }}
                                </td>
                                <td>
                                    <a href="{{ route('admin.videos.edit', $video->id) }}" class="btn btn-sm btn-info">Edit</a>
                                    <form method="POST" action="{{ route('admin.videos.destroy', $video->id) }}">
                                        @csrf
                                        {{ method_field('DELETE') }}
                                        <input type="submit" value="Delete" onclick="return confirm('Weet u zeker dat u de video wilt verwijderen?')"
                                               class="btn btn-sm btn-danger" />
                                    </form>
                                </td>
                            </tr>
                            @empty
                        <tr>
                            <td colspan="2">
                                Geen video's gevonden.
                            </td>
                        </tr>
                            @endforelse
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
