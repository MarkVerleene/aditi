@extends('layouts.adminlayout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Nieuwe info toevoegen</div>

                <div class="card-body">

                    <form method="POST" action="{{route('admin.info.store')}}" enctype="multipart/form-data" name="infoForm">
                        @csrf
                        <div class="field">
                            Titel:
                            <input type="text" name="title" class="form-control" />
                            Tekst:
                            <input type="text" name="text" class="form-control" />

                            <br />

                            <input type="submit" value="Bewaar" id="createInfoFormSubmitBtn" class="btn btn-primary"/>
                            <a value="Annuleer" class="btn btn-primary btn-close" href="{{route('admin.info.index')}}"> Annuleer</a>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
