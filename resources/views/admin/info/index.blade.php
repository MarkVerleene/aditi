@extends('layouts.adminlayout')

@section('content')
<div class="container">
    {{--<form>
        <div class="form-group">
            <textarea class="form-control" id="info-body"></textarea>
        </div>
        <button type="submit" class="btn btn-secondary">Opslaan</button>
    </form>--}}

    {{--<textarea id="my-editor" name="content" class="form-control">{!! old('content', 'test editor content') !!}</textarea>
    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
    <script>
        var options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
        };
    </script>--}}
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Info</div>

                <div class="card-body">

                    <a href="{{route('admin.info.create')}}" class="btn btn-sm btn-primary">Nieuwe info toevoegen</a>
                    <br /><br />


                    <table class="table">
                        <tr>
                            <td>
                                Titel
                            </td>
                            <td>
                                Tekst
                            </td>
                        </tr>
                        @forelse($infos as $info)
                            <tr>
                                <td>
                                    {{ $info->title }}
                                </td>
                                <td>
                                    {{ $info->text }}
                                </td>
                                <td>
                                    <a href="{{ route('admin.info.edit', $info->id) }}" class="btn btn-sm btn-info">Edit</a>
                                    <form method="POST" action="{{ route('admin.info.destroy', $info->id) }}">
                                        @csrf
                                        {{ method_field('DELETE') }}
                                        <input type="submit" value="Delete" onclick="return confirm('Weet u zeker dat u de info wilt verwijderen?')"
                                               class="btn btn-sm btn-danger" />
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="2">
                                    Geen info gevonden.
                                </td>
                            </tr>
                        @endforelse
                    </table>

                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('scriptsToLoad')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace( 'info-body' );
    </script>

@endsection
