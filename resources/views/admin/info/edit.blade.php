@extends('layouts.adminlayout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Video aanpassen</div>

                <div class="card-body">

                    <form method="POST" action="{{ route('admin.videos.update', $video->id) }}">
                        {{ method_field('PUT') }}
                        @csrf
                        Title:
                        <input type="text" name="title" value="{{$video->title}}" class="form-control" />
                        Categorie id:
                        <input type="text" name="categorie_id" value="{{$video->categorie_id}}" class="form-control" />
                        Beschrijving:
                        <input type="text" name="description" value="{{$video->description}}" class="form-control" />
                        Video URL:
                        <input type="text" name="movie_url" value="{{$video->movie_url}}" class="form-control" />
                        Rating:
                        <input type="text" name="rating" value="{{$video->rating}}" class="form-control" />

                        <br />

                        <input type="submit" value="Bewaar" class="btn btn-primary"/>
                        <a value="Annuleer" class="btn btn-primary btn-close" href="{{route('admin.videos.index')}}"> Annuleer</a>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
