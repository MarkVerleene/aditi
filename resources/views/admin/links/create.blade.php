@extends('layouts.adminlayout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Nieuwe link toevoegen</div>

                <div class="card-body">

                    <form method="POST" action="{{route('admin.links.store')}}" enctype="multipart/form-data" name="linkForm">
                        @csrf
                        <div class="field">
                            Organisatie:
                            <input type="text" name="organisatie" class="form-control" />
                            Website:
                            <input type="text" name="websitelink" class="form-control" />

                            <br />

                            <input type="submit" value="Bewaar" id="createLinkFormSubmitBtn" class="btn btn-primary"/>
                            <a value="Annuleer" class="btn btn-primary btn-close" href="{{route('admin.links.index')}}"> Annuleer</a>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
