@extends('layouts.adminlayout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Links</div>

                <div class="card-body">

                    <a href="{{route('admin.links.create')}}" class="btn btn-sm btn-primary">Nieuwe link toevoegen</a>
                    <br /><br />


                    <table class="table">
                        <tr>
                            <td>
                                Organisatie
                            </td>
                            <td>
                                Website
                            </td>
                        </tr>
                        @forelse($links as $link)
                            <tr>
                                <td>
                                    {{ $link->organisatie }}
                                </td>
                                <td>
                                    {{ $link->websitelink }}
                                </td>
                                <td>
                                    <a href="{{ route('admin.links.edit', $link->id) }}" class="btn btn-sm btn-info">Edit</a>
                                    <form method="POST" action="{{ route('admin.links.destroy', $link->id) }}">
                                        @csrf
                                        {{ method_field('DELETE') }}
                                        <input type="submit" value="Delete" onclick="return confirm('Weet u zeker dat u de link wilt verwijderen?')"
                                               class="btn btn-sm btn-danger" />
                                    </form>
                                </td>
                            </tr>
                            @empty
                        <tr>
                            <td colspan="2">
                                Geen links gevonden.
                            </td>
                        </tr>
                            @endforelse
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
