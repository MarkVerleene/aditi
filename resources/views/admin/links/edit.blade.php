@extends('layouts.adminlayout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Link aanpassen</div>

                <div class="card-body">

                    <form method="POST" action="{{ route('admin.links.update', $link->id) }}">
                        {{ method_field('PUT') }}
                        @csrf
                        Organisatie:
                        <input type="text" name="organisatie" value="{{$link->organisatie}}" class="form-control" />
                        Websitelink:
                        <input type="text" name="websitelink" value="{{$link->websitelink}}" class="form-control" />

                        <br />

                        <input type="submit" value="Bewaar" class="btn btn-primary"/>
                        <a value="Annuleer" class="btn btn-primary btn-close" href="{{route('admin.links.index')}}"> Annuleer</a>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
