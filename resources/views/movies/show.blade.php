@extends('layouts.mainlayout')

@section('title')
    Show
@endsection

@section('content')
    <h1 class="title">{{ $movie->title }}</h1>
    <div class="content">
        {{ $movie->description }}

        <iframe id="myVideo" frameborder="0" src="{{$movie->movie_url}}?rel=0&controls=0&autoplay=1"
                allow="autoplay; encrypted-media; " allowfullscreen
                style="position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">
        </iframe>

    </div>
    {{--<iframe width="640" height="360" src="https://www.youtube.com/embed/l482T0yNkeo" frameborder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>--}}
    @endsection
