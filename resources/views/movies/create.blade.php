@extends('layouts.mainlayout')

@section('title')
    Upload file
@endsection

@section('content')
    <h1>Upload file</h1>
    <form method="POST" action="/movies" enctype="multipart/form-data" name="videoFileForm">
        @csrf
        <div class="field">
            <input type="file" name="videoFile" value="" class="input">
            <input type=text class="input" name="title" placeholder="Movie title">
        <button type="submit" name="VideoFileFormUploadSubmitBtn" class="button is-link">Upload file</button>
        </div>
    </form>

@endsection

