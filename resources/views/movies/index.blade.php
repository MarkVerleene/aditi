@extends('layouts.mainlayout')

@section('title')
    Movies
@endsection

@section('content')

    <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>

                <div class="modal-body">
                    {{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>--}}

                        <iframe id="myVideo" frameborder="0" src=""
                                allow="autoplay; encrypted-media; " allowfullscreen
                                style="position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">
                        </iframe>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>


    <h1 class="title">Overzicht video's</h1>


    <div class="row moviesPerCategory">

        @foreach ($randomMoviesByRating as $movie)

            <div class="col">
                <div class="media">
                    <div class="media-body">
                        <a href="#" data-toggle="modal" data-target="#videoModal" data-theVideo="{{$movie->movie_url}}" title="{{$movie->description}}">
                            <img class="img-thumbnail videosPerCategoryImage" id="playVidImage" src="/img/playVidImage.jpg" alt="{{$movie->description}}">
                        </a>
                    </div>
                </div>
                <a href="#" data-toggle="modal" data-target="#videoModal" data-theVideo="{{$movie->movie_url}}"><h4>{{$movie->title}}</h4></a>
            </div>

        @endforeach
    </div>

{{--<a href="\movies\create">UPLOAD NEW FILE</a>--}}

@endsection

@section('scriptsToLoad')
    {{--<script>
        // Get the modal
        var modal = document.getElementById('videoModal');

        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var vid = document.getElementById('myImg');
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        img.onclick = function(){
            modal.style.display = "block";
            modalImg.src = this.src;
            captionText.innerHTML = this.alt;
        };

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }
    </script>--}}

    <script>
        function autoPlayYouTubeModal(){
            var trigger = $("body").find('[data-toggle="modal"]');
            trigger.click(function() {
                var theModal = $(this).data( "target" ),
                    videoSRC = $(this).attr( "data-theVideo" ),
                    videoSRCauto = videoSRC+"?rel=0&autoplay=1" ; //&controls=0
                $(theModal+' iframe').attr('src', videoSRCauto);
                $(theModal+' button.close').click(function () {
                    $(theModal+' iframe').attr('src', videoSRC);
                });
            });
        }

        $(document).ready(function(){
            autoPlayYouTubeModal();
            $("#videoModal").on("hidden.bs.modal",function(){
                $("#myVideo").attr("src","#");
            })
        });


        /*$(document).ready(function(){
            $("#videoModal").on("hidden.bs.modal",function(){
                $("#myVideo").attr("src","#");
            })
        })*/

        $("#myVideo").on("ended", function() {

            $('#videoModal').modal('hide');
        });

    </script>

    {{--<script>
        $("#myVideo").on("ended", function() {
            //TO DO: Your code goes here...
            $('#videoModal').modal('hide');// hides the modal
        });


        $('#videoModal').on('hide.bs.modal', function(e) {
            var $if = $(e.delegateTarget).find('iframe');
            var src = $if.attr("src");
            $if.attr("src", '/empty.html');
            $if.attr("src", src);
        });
    </script>--}}

@endsection

{{--
<h3>HTML5 Video</h3>
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#html5Video">
    Launch HTML5 video
</button>

<!-- Modal -->
<div class="modal fade" id="html5Video" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <video id="htmlVideo" width="100%" controls>
                    <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
                    <source src="https://www.w3schools.com/html/mov_bbb.ogg" type="video/ogg">
                    Your browser does not support HTML5 video.
                </video>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>--}}
