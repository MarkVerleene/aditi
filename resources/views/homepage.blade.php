@extends('layouts.mainlayout')

@section('title')
    SeksEd
@endsection

@section('header')
    <div class="guestHeader">
        <h1>Seks en dergelijke</h1>

        <h4>Over seks enzo</h4>
        <a href="/info">
            <button class="btn-info">Info</button>
        </a>
        @auth
            <a href="/home">
                <button class="btn-info">Terug naar admin</button>
            </a>
            @endauth
    </div>


@endsection

@section('content')
    {{--THE MODAL--}}
    <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">

                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <video autoplay id="video" src="" type="video/mp4">
                    </video>
                </div>

            </div>
        </div>
    </div>

    {{--VIDEO CATEGORIES--}}
    <h1>Video categorieën</h1>

    <div class="row moviecategories">
        @foreach($categories as $category)
            <div class="col">
                <button name="test" class="videoCategorieButton btn btn-secondary btnsound" value="{{$category->audio}}">{{$category->name}}</button>
                <a role="button" href="#collapseCategory" data-categoryId="{{$category->id}}" data-categoryName="{{$category->name}}" class="category" data-toggle="button">
                    <img class="videoCategoryImage" tabindex="0" src="{{$category->icon}}"/>
                </a>
            </div>
        @endforeach
    </div>

    {{--COLLAPSE: VIDEOS PER CATEGORY--}}
    {{--    toggle manueel via JS - geef cat door via data-categoryId--}}

    <div class="collapse" id="collapseCategory">
        <h1>Video categorie: </h1>
        <div class="row moviesPerCategory">

        </div>
    </div>

    {{--EDUCATIEVE AFBEELDINGEN--}}
    <h1>Educatieve afbeeldingen</h1>

    <div class="row picturecategories">
        <div class="col">
            <button name="test" class="pictureCategorieButton btn btn-secondary btnsound" id="btnVagina" value="{{asset("sounds/sos.mp3")}}">Vagina</button>
            <a href="/vagina">
                <img class="pictureCategoryImage" src="{{asset('/img/seksualiteit/vagina 2.png')}}"/>
            </a>
        </div>
        <div class="col">
            <button name="test" class="pictureCategorieButton btn btn-secondary btnsound" id="btnPenis" value="{{asset('sounds/sos.mp3')}}">Penis</button>
            <a href="/penis">
                <img class="pictureCategoryImage" src="{{asset('/img/seksualiteit/penis onbesneden.png')}}"/>
            </a>
        </div>
    </div>

@endsection

@section('scriptsToLoad')

{{--    soundboard module ("sb")--}}
    <script src="/js/soundboard.js"></script>
    <script>
        $(document).ready(initSoundboard);
    </script>

{{--    collapse html opbouwen voor video's van de geselecteerde categorie --}}
    <script src="/js/buildHtmlVideosPerCategory.js"></script>
    <script>
        $(document).ready(initBuildHtmlVideosPerCategory);
    </script>

{{--    modal/video behaviour --}}
    <script src="/js/playVideoInModal.js"></script>
    <script>
        $(document).ready(initPlayVideoInModal);
    </script>
@endsection

